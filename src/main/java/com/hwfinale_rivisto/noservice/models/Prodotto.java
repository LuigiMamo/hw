package com.hwfinale_rivisto.noservice.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="prodotto")
public class Prodotto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long prodotto_id;
	@Column
	private String codice;
	@Column
	private String nome;
	@Column
	private int prezzo;
	@Column
	private int quantita;
	
	@ManyToMany
	@JoinTable(name="preventivo_prodotto",
				joinColumns = {@JoinColumn(name="prodotto_rif", referencedColumnName = "prodotto_id")},
				inverseJoinColumns = {@JoinColumn(name="preventivo_rif", referencedColumnName = "preventivo_id")})
	private List<Preventivo> preventivi;

	public Prodotto() {
		
	}

	public Prodotto(String codice, String nome, int prezzo, int quantita) {
		super();
		this.codice = codice;
		this.nome = nome;
		this.prezzo = prezzo;
		this.quantita = quantita;
	}

	public long getProdotto_id() {
		return prodotto_id;
	}
	
	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(int prezzo) {
		this.prezzo = prezzo;
	}

	public List<Preventivo> getPreventivi() {
		return preventivi;
	}

	public void setPreventivi(List<Preventivo> preventivi) {
		this.preventivi = preventivi;
	}

	public int getQuantita() {
		return quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	
	
	
	
}
