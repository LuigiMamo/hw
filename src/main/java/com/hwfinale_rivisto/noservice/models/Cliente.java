package com.hwfinale_rivisto.noservice.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long cliente_id;
	@Column
	private String nome_azienda;
	@Column
	private String indirizzo;
	
	@OneToMany(mappedBy = "cliente")
	@JsonManagedReference
	private List<Preventivo> preventivi;
	
	public Cliente () {
		
	}
	
	public Cliente(String nome_azienda, String indirizzo) {
		super();
		this.nome_azienda = nome_azienda;
		this.indirizzo = indirizzo;
	}

	public long getCliente_id() {
		return cliente_id;
	}

	public String getNome_azienda() {
		return nome_azienda;
	}

	public void setNome_azienda(String nome_azienda) {
		this.nome_azienda = nome_azienda;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public List<Preventivo> getPreventivi() {
		return preventivi;
	}

	public void setPreventivi(List<Preventivo> preventivi) {
		this.preventivi = preventivi;
	}
	
	
	
}
