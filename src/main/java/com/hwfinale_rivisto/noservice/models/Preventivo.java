package com.hwfinale_rivisto.noservice.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "preventivo_id")
@Entity
@Table(name="preventivo")
public class Preventivo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long preventivo_id;
	@Column
	private String note;
	@Column
	private String emittente;
	@Column
	private String indirizzo_emittente;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="cliente_rif")
	@JsonBackReference
	private Cliente cliente;
	
	@ManyToMany
	@JoinTable(name="preventivo_prodotto",
				joinColumns = {@JoinColumn(name="preventivo_rif", referencedColumnName = "preventivo_id")},
				inverseJoinColumns = {@JoinColumn(name="prodotto_rif", referencedColumnName = "prodotto_id")})
	private List<Prodotto> prodotti;
	
	public Preventivo() {
		
	}

	public Preventivo(String note, String emittente, String indirizzo_emittente) {
		super();
		this.note = note;
		this.emittente = emittente;
		this.indirizzo_emittente = indirizzo_emittente;
	}

	public long getPreventivo_id() {
		return preventivo_id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getEmittente() {
		return emittente;
	}

	public void setEmittente(String emittente) {
		this.emittente = emittente;
	}

	public String getIndirizzo_emittente() {
		return indirizzo_emittente;
	}

	public void setIndirizzo_emittente(String indirizzo_emittente) {
		this.indirizzo_emittente = indirizzo_emittente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Prodotto> getProdotti() {
		return prodotti;
	}

	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}
	
	
	

}
