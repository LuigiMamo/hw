package com.hwfinale_rivisto.noservice.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hwfinale_rivisto.noservice.exceptions.ResourceNotFoundException;
import com.hwfinale_rivisto.noservice.models.Cliente;
import com.hwfinale_rivisto.noservice.repositories.ClienteRepo;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteRepo repo;
	
	@PostMapping("/insert")
	public Cliente insert(@Valid @RequestBody Cliente c) {
		return repo.save(c);
	}
	
	@GetMapping("/list")
	public List<Cliente> findAll(){
		return repo.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Cliente> findById(@PathVariable long id) {
		return repo.findById(id);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Cliente> update(@PathVariable long id,
			@Valid @RequestBody Cliente c) throws ResourceNotFoundException{
		Cliente cliente = repo.findById(id).
				orElseThrow(() -> new ResourceNotFoundException("Cliente non trovato per l'id:" + id));
		cliente.setNome_azienda(c.getNome_azienda());
		cliente.setIndirizzo(c.getIndirizzo());
		final Cliente updateCliente = repo.save(cliente);
		return ResponseEntity.ok(updateCliente);
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Boolean> delete(@PathVariable long id)
	throws ResourceNotFoundException{
		Cliente cliente = repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Cliente non trovato per l'id:" + id));
		
		repo.delete(cliente);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", true);
		return response;
	}
}
