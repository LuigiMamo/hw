package com.hwfinale_rivisto.noservice.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hwfinale_rivisto.noservice.exceptions.ResourceNotFoundException;
import com.hwfinale_rivisto.noservice.models.Prodotto;
import com.hwfinale_rivisto.noservice.repositories.ProdottoRepo;

@RestController
@RequestMapping("/prodotto")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProdottoController {
	
	@Autowired
	private ProdottoRepo repo;
	
	@PostMapping("/insert")
	public Prodotto insert(@Valid @RequestBody Prodotto p) {
		return repo.save(p);
	}
	
	@GetMapping("/list")
	public List<Prodotto> findAll(){
		return repo.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Prodotto> findById(@PathVariable long id) {
		return repo.findById(id);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Prodotto> update(@PathVariable long id,
			@Valid @RequestBody Prodotto p) throws ResourceNotFoundException{
		Prodotto prod = repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("prodotto non trovate per l'id:" + id));
		
		prod.setNome(p.getNome());
		prod.setCodice(p.getCodice());
		prod.setPrezzo(p.getPrezzo());
		prod.setQuantita(p.getQuantita());
		final Prodotto updateProd = repo.save(prod);
		return ResponseEntity.ok(updateProd);
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Boolean> delete(@PathVariable long id)
	throws ResourceNotFoundException{
		Prodotto prod = repo.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("prodotto non trovato per l'id:"+ id));
		repo.delete(prod);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", true);
		return response;
	}

}
