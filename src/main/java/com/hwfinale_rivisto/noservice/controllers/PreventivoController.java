package com.hwfinale_rivisto.noservice.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hwfinale_rivisto.noservice.exceptions.ResourceNotFoundException;
import com.hwfinale_rivisto.noservice.models.Cliente;
import com.hwfinale_rivisto.noservice.models.Preventivo;
import com.hwfinale_rivisto.noservice.models.Prodotto;
import com.hwfinale_rivisto.noservice.repositories.ClienteRepo;
import com.hwfinale_rivisto.noservice.repositories.PreventivoRepo;
import com.hwfinale_rivisto.noservice.repositories.ProdottoRepo;

@RestController
@RequestMapping("/preventivo")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PreventivoController {
	
	@Autowired
	private PreventivoRepo repo;
	@Autowired
	private ClienteRepo cliente;
	@Autowired
	private ProdottoRepo prod;
	
	@PostMapping("/insert")
	public Preventivo insert(@Valid @RequestBody Preventivo p) {
		return repo.save(p);
	}
	
	@GetMapping("/list")
	public List<Preventivo> findAll(){
		return repo.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Preventivo> findById(@PathVariable long id) {
		return repo.findById(id);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Preventivo> update(@PathVariable long id,
			@Valid @RequestBody Preventivo p) throws ResourceNotFoundException{
		Preventivo temp = repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("preventivo non trovato per l'id:" + id));
		
		temp.setEmittente(p.getEmittente());
		temp.setIndirizzo_emittente(p.getIndirizzo_emittente());
		temp.setNote(p.getNote());
		
		final Preventivo updatePrev = repo.save(temp);
		return ResponseEntity.ok(updatePrev);
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Boolean> delete(@PathVariable long id) 
			throws ResourceNotFoundException{
		Preventivo temp = repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Preventivo non trovato per l'id:"+id));
		repo.delete(temp);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", true);
		return response;
	}
	
	@PutMapping("/associa_cliente/{id}/{cliente_id}")
	public ResponseEntity<Preventivo> associaCliente(@PathVariable long cliente_id,
			@PathVariable long id, @Valid @RequestBody Preventivo p)
			throws ResourceNotFoundException{
		Preventivo temp = repo.findById(id)	
				.orElseThrow(() -> new ResourceNotFoundException("Nessun preventivo trovato per l'id:"+id));
		Cliente asso = cliente.findById(cliente_id)
				.orElseThrow(() -> new ResourceNotFoundException("Nessun cliente trovato per l'id:"+cliente_id));
		temp.setCliente(asso);
		final Preventivo updatePrev = repo.save(temp);
		return ResponseEntity.ok(updatePrev);
	}
	
	@PutMapping("associa_prodotto/{prev_id}/{prod_id}")
	public ResponseEntity<Preventivo> addProd(@PathVariable long prod_id,
			@PathVariable long prev_id, @Valid @RequestBody Preventivo p)
	throws ResourceNotFoundException{
		Preventivo temp = repo.findById(prev_id)
				.orElseThrow(()-> new ResourceNotFoundException("nessun preventivo con questo id:"+prev_id));
		Prodotto prodotto = prod.findById(prod_id)
				.orElseThrow(() -> new ResourceNotFoundException("nessun prodotto con questo id:"+prod_id));
		List<Prodotto> pList = new ArrayList<Prodotto>();
		pList.add(prodotto);
		temp.setProdotti(pList);
		final Preventivo updatePrev = repo.save(temp);
		return ResponseEntity.ok(updatePrev);
	}
	
}
