package com.hwfinale_rivisto.noservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoserviceApplication.class, args);
	}

}
