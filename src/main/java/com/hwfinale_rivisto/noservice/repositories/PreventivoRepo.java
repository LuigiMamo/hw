package com.hwfinale_rivisto.noservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hwfinale_rivisto.noservice.models.Preventivo;
@Repository
public interface PreventivoRepo extends JpaRepository<Preventivo, Long>{

}
