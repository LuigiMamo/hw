package com.hwfinale_rivisto.noservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hwfinale_rivisto.noservice.models.Cliente;
@Repository
public interface ClienteRepo extends JpaRepository<Cliente, Long> {

}
